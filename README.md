# PNG Compressor Benchmarks
Used to test some command-line compression tools against each other, for this [blog post](https://sburris.xyz/posts/best-png-compression/).

## Installation and Usage
I recommend [Python Poetry](https://python-poetry.org/) for installing the dependencies.
Run `poetry install` and then run the graph-generating script with `python3 graph.py`.

## Compressors
I used this to test:
* [OptiPNG 0.7.7](http://optipng.sourceforge.net/)
* [Oxipng 3.0.1](https://lib.rs/crates/oxipng)
* [Pngcrush 1.8.13](https://pmt.sourceforge.io/pngcrush/)
* [pngquant 2.13.0](https://pngquant.org/)

## Results
![File size](build/size.svg)
![Execution time](build/time.svg)
