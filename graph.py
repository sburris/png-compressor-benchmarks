import pandas as pd
import plotly.express as px

# Load data
df = pd.read_csv("data/data_averaged.csv")

# Create plots
time = px.bar(df,
             x="program",
             y="time",
             labels={
                "time": "time (s)",
             },
             color="image",
             title="PNG Compressor Performance")

size = px.bar(df,
             x="program",
             y="bytes",
             labels={
                 "bytes": "size (bytes)",
             },
             color="image",
             title="PNG Compressor Performance")

# Export
time.write_image("build/time.svg")
time.write_html("build/time.html")
size.write_image("build/size.svg")
time.write_html("build/size.html")

# Display in browser
# time.show()
# size.show()
